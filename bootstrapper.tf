# main.tf

# Uses DOCKER_PASSWORD environment variable
provider "dockerhub" {
  username = var.docker_username
}

resource "dockerhub_token" "bootstrapper" {
  label  = "bootstrapper - ${terraform.workspace}"
  scopes = ["repo:read"]
}

provider "hcp" {
  client_id     = var.hcp_client_id
  client_secret = var.hcp_client_secret
}

resource "hcp_vault_secrets_app" "bootstrapper_app" {
  app_name    = "bootstrapper-${terraform.workspace}"
  description = timestamp()
}

provider "hcloud" {
  token = var.hcloud_token
}

data "http" "hetzner_api_images" {
  url = "https://api.hetzner.cloud/v1/images?type=snapshot&status=available&name=${var.hcloud_image_name}"

  request_headers = {
    Authorization = "Bearer: ${var.hcloud_token}"
  }
}

data "hcloud_image" "install_image" {
  with_selector     = "name=${var.hcloud_image_name}"
  with_architecture = startswith(var.server_type, "cax") ? "arm" : "x86"
}

data "hcloud_ssh_key" "authorized_keys" {
  name = keys(var.ssh_authorized_keys)[0]
}

data "hcloud_network" "internal" {
  name = "internal"
}

locals {
  ip_range_list = split(".", split("/", data.hcloud_network.internal.ip_range)[0])
  subnet_cidr = merge(
    { cluster = join(".", [local.ip_range_list[0], local.ip_range_list[1],
    var.network_thirds["cluster"], "0/24"]) },
    { bootstrapper = join(".", [local.ip_range_list[0], local.ip_range_list[1],
    var.network_thirds["bootstrapper"], "0/24"]) },
    { workers = join(".", [local.ip_range_list[0], local.ip_range_list[1],
    var.network_thirds["workers"], "0/24"]) }
  )
  subnet_ipam = merge(
    { cluster = replace(local.subnet_cidr["cluster"], ".0/24", ".") },
    { bootstrapper = replace(local.subnet_cidr["bootstrapper"], ".0/24", ".") },
    { workers = replace(local.subnet_cidr["workers"], ".0/24", ".") }
  )
}

output "subnet_cidr" {
  value = local.subnet_cidr
}

output "subnet_ipam" {
  value = local.subnet_ipam
}

resource "hcloud_network_subnet" "bootstrapper" {
  count        = length(var.bootstrapper_nodes) >= 1 ? 1 : 0
  network_id   = data.hcloud_network.internal.id
  type         = "cloud"
  network_zone = var.network_zone
  ip_range     = local.subnet_cidr["bootstrapper"]
}

resource "hcloud_firewall" "bootstrapper_firewall" {
  count = length(var.bootstrapper_nodes) >= 1 ? 1 : 0
  name  = "bootstrapper-firewall"
  rule {
    direction  = "in"
    protocol   = "tcp"
    port       = "any"
    source_ips = var.firewall_allow_cidrs
  }
}

data "template_file" "bootstrapper_cloud_config" {
  template = file("cloudinit/bootstrapper-cloud-config.yaml")

  vars = {
    hcloud_token        = var.node_hcloud_token
    ansible_repo        = var.ansible_pull_repo
    ansible_playbook    = var.ansible_pull_playbook
    ansible_checkout    = var.ansible_pull_checkout
    ansible_tags        = var.ansible_pull_tags
    hcp_client_id       = var.hcp_client_id
    hcp_client_secret   = var.hcp_client_secret
    hcp_app_name        = var.hcp_app_name
    hcp_organization_id = var.hcp_organization_id
    hcp_project_id      = var.hcp_project_id
    docker_username     = var.docker_username
    docker_token        = dockerhub_token.bootstrapper.token
    ldap_bind_dn        = var.ldap_bind_dn
    ldap_bind_pwd       = var.ldap_bind_pwd
    ldap_org_dn         = var.ldap_org_dn
    ldap_url            = var.ldap_url
    ldap_policies       = var.ldap_vault_group_policies
    location            = var.location
    server_type         = var.server_type
  }
}

data "cloudinit_config" "bootstrapper" {
  gzip          = false
  base64_encode = false

  part {
    content_type = "text/cloud-config"
    filename     = "terraform.yaml"
    content      = data.template_file.bootstrapper_cloud_config.rendered
  }
}

resource "random_pet" "bootstrapper" {
  count = 1

  keepers = {
    image             = data.hcloud_image.install_image.id
    host_location     = var.location
    host_type         = var.server_type
    cloudinit_payload = data.cloudinit_config.bootstrapper.rendered
    ssh_keys          = data.hcloud_ssh_key.authorized_keys.id
    ip                = join("", [local.subnet_ipam["bootstrapper"], tostring(count.index + 2)])
  }
}

resource "hcloud_server" "bootstrappers" {
  count        = length(var.bootstrapper_nodes)
  name         = random_pet.bootstrapper[count.index].id
  server_type  = random_pet.bootstrapper[count.index].keepers.host_type
  location     = random_pet.bootstrapper[count.index].keepers.host_location
  image        = random_pet.bootstrapper[count.index].keepers.image
  labels       = merge(var.bootstrapper_nodes[count.index]["labels"],
                       { private_ip: random_pet.bootstrapper[count.index].keepers.ip })
  ssh_keys     = [random_pet.bootstrapper[count.index].keepers.ssh_keys]
  user_data    = random_pet.bootstrapper[count.index].keepers.cloudinit_payload
  firewall_ids = [hcloud_firewall.bootstrapper_firewall[0].id]

  network {
    network_id = data.hcloud_network.internal.id
    ip         = random_pet.bootstrapper[count.index].keepers.ip
    alias_ips  = []
  }

  public_net {
    ipv4_enabled = contains(keys(var.bootstrapper_nodes[count.index]["labels"]),
    "bootstrapper") ? true : false
    ipv6_enabled = false
  }
}
