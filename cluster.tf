# main.tf

resource "hcloud_network_subnet" "cluster" {
  count        = length(var.cluster_nodes) >= 1 ? 1 : 0
  network_id   = data.hcloud_network.internal.id
  type         = "cloud"
  network_zone = var.network_zone
  ip_range     = local.subnet_cidr["cluster"]
}

resource "hcloud_firewall" "cluster_firewall" {
  count = length(var.cluster_nodes) >= 1 ? 1 : 0
  name  = "cluster-firewall"
  rule {
    direction  = "in"
    protocol   = "tcp"
    port       = "any"
    source_ips = ["10.0.0.0/8"]
  }
}

data "template_file" "cluster_cloud_config" {
  template = file("cloudinit/cluster-cloud-config.yaml")

  vars = {
    hcloud_token     = var.node_hcloud_token
    ansible_repo     = var.ansible_pull_repo
    ansible_playbook = var.ansible_pull_playbook
    ansible_checkout = var.ansible_pull_checkout
    ansible_tags     = var.ansible_pull_tags
    location         = var.cluster_location != "" ? var.cluster_location : var.location
    server_type      = var.cluster_server_type != "" ? var.cluster_server_type : var.server_type
    bootstrapper_ip  = length(random_pet.bootstrapper) == 0 ? "" : random_pet.bootstrapper[0].keepers.ip
  }
}

data "cloudinit_config" "cluster_nodes" {
  gzip          = false
  base64_encode = false

  part {
    content_type = "text/cloud-config"
    filename     = "terraform.yaml"
    content      = data.template_file.cluster_cloud_config.rendered
  }

  part {
    content_type = "text/x-shellscript"
    content      = file("cloudinit/pulpify_repos.sh")
    filename     = "pulpify-repos.sh"
  }

  part {
    content_type = "text/x-shellscript"
    content      = file("cloudinit/setup_dns.sh")
    filename     = "setup-dns.sh"
  }
}

resource "random_pet" "cluster" {
  count = length(var.cluster_nodes)

  keepers = {
    image             = data.hcloud_image.install_image.id
    host_location     = var.cluster_location != "" ? var.cluster_location : var.location
    host_type         = var.cluster_server_type != "" ? var.cluster_server_type : var.server_type
    cloudinit_payload = data.cloudinit_config.cluster_nodes.rendered
    ssh_keys          = data.hcloud_ssh_key.authorized_keys.id
    ip                = join("", [local.subnet_ipam["cluster"], tostring(count.index + 2)])
  }
}

resource "hcloud_server" "cluster_nodes" {
  count        = length(var.cluster_nodes)
  name         = random_pet.cluster[count.index].id
  server_type  = random_pet.cluster[count.index].keepers.host_type
  location     = random_pet.cluster[count.index].keepers.host_location
  image        = random_pet.cluster[count.index].keepers.image
  labels       = merge(var.cluster_nodes[count.index]["labels"],
                       { private_ip: random_pet.cluster[count.index].keepers.ip })
  ssh_keys     = [random_pet.cluster[count.index].keepers.ssh_keys]
  user_data    = random_pet.cluster[count.index].keepers.cloudinit_payload
  firewall_ids = [hcloud_firewall.cluster_firewall[0].id]

  network {
    network_id = data.hcloud_network.internal.id
    ip         = random_pet.cluster[count.index].keepers.ip
    alias_ips  = []
  }

  public_net {
    ipv4_enabled = contains(keys(var.cluster_nodes[count.index]["labels"]),
    "cluster") ? true : false
    ipv6_enabled = false
  }
}
