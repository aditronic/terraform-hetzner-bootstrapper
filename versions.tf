# versions.tf

terraform {
  cloud {}
  required_providers {
    hcloud    = { source = "hetznercloud/hcloud", version = "~> 1.44" }
    sys       = { source = "neuspaces/system", version = "~> 0.4" }
    cloudinit = { source = "hashicorp/cloudinit", version = "~> 2.3" }
    vault     = { source = "hashicorp/vault", version = "~> 3.23" }
    dockerhub = { source = "BarnabyShearer/dockerhub", version = ">= 0.0.15" }
  }

  required_version = "~> 1.6"
}
