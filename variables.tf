# variables.tf

# export TF_VAR_hcloud_token="<Hetzner API token>"
variable "hcloud_token" {
  description = "Hetzner Cloud API Token"
  type        = string
  sensitive   = true
}

variable "server_type" {
  description = "Server type"
  type        = string
}

variable "cluster_server_type" {
  description = "Server type"
  type        = string
  default     = ""
}

variable "network_zone" {
  description = "Server network zone"
  type        = string
}

variable "ssh_authorized_keys" {
  description = "Authorized keys to add for login"
  type        = map(any)
}

variable "location" {
  description = "Server location"
  type        = string
}

variable "cluster_location" {
  description = "Location for server nodes"
  type        = string
  default     = ""
}

variable "network_thirds" {
  description = "Define third triplet for the specified networks"
  type        = map(any)
  default = { "bootstrapper" : 100,
    "cluster" : 1,
  "workers" : 10 }
}

variable "bootstrapper_nodes" {
  description = "Server configuration map"
  type        = list(any)
  default = [
    {
      labels = {
        datacenter   = "h1",
        bootstrapper = "server"
      }
    }
  ]
}

variable "cluster_nodes" {
  description = "Cluster configuration map"
  type        = list(any)
  default = [{ "labels" : { "vault" : "server", "consul" : "server" },
    "ip" = "10.0.0.2" }
  ]
}

variable "node_hcloud_token" {
  description = "Token used to access Hetzner Cloud from the machine"
  type        = string
}

variable "hcloud_image_name" {
  description = "Name of install image"
  type        = string
  default     = "alma-9-arm64"
}

variable "firewall_allow_cidrs" {
  description = "List of CIDRs that can access vault"
  type        = set(string)
}

variable "ansible_pull_repo" {
  description = "Link to ansible repo to run with ansible-pull"
  type        = string
  default     = "https://github.com/ansible/tower-example"
}

variable "ansible_pull_playbook" {
  description = "Name of playbook to run in ansible repo"
  type        = string
  default     = "helloworld.yml"
}

variable "ansible_pull_checkout" {
  description = "Refspec to checkout in ansible repo"
  type        = string
  default     = "main"
}

variable "ansible_pull_tags" {
  description = "Tags to run in ansible pull"
  type        = string
  default     = "all"
}

variable "hcp_client_id" {
  type = string
}

variable "hcp_client_secret" {
  type      = string
  sensitive = true
}

variable "hcp_app_name" {
  type = string
}

variable "hcp_organization_id" {
  type = string
}

variable "hcp_project_id" {
  type = string
}

variable "docker_username" {
  type = string
}

variable "ldap_vault_group_policies" {
  description = "Policies to attach to the vault ldap groups"
  type        = string
}

variable "ldap_url" {
  description = "URL to LDAP server used for vault auth method"
  type        = string
}

variable "ldap_org_dn" {
  description = "DN for the ldap organization"
  type        = string
}

variable "ldap_bind_dn" {
  description = "DN to use to bind to the LDAP server"
  type        = string
}

variable "ldap_bind_pwd" {
  description = "Password associated with ldap_bind_dn"
  type        = string
  sensitive   = true
}
