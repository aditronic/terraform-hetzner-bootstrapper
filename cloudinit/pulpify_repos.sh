#!/bin/sh

while [ ! -f "/var/lib/misc/dns-server" ]; do
  sleep 3
done

DNS=$(cat /var/lib/misc/dns-server)

cd /etc/yum.repos.d || exit
for i in alma*.repo ; do
  sed 's/^mirrorlist.*$//' "$i" | sed 's/^# baseurl/baseurl/' > /tmp/"$i"
  sed "s#https://repo.almalinux.org/#http://$DNS:8080/pulp/content/#" < /tmp/"$i" > "$i"
  rm /tmp/"$i"
done
for i in epel*.repo ; do
  sed 's/^metalink.*$//' "$i" | sed 's/^#baseurl/baseurl/' > /tmp/"$i"
  sed "s#https://download.example/#http://$DNS:8080/pulp/content/#" < /tmp/"$i" > "$i"
  rm /tmp/"$i"
done
