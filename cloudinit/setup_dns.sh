#!/bin/sh

while [ ! -f "/var/lib/misc/dns-server" ]; do
  sleep 3
done

DNS=$(cat /var/lib/misc/dns-server)

sed -i '/\[main\]/a\dns=none' /etc/NetworkManager/NetworkManager.conf
systemctl restart NetworkManager
echo "nameserver $DNS" > /etc/resolv.conf
